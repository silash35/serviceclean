import Head from "next/head";

import About from "../components/about";
import Contact from "../components/contact";
import Header from "../components/header";
import Home from "../components/home";
import ScrollBack from "../components/scrollBack";
import Storelist from "../components/storelist";

export default function HomePage() {
  return (
    <>
      <Head>
        <title>Service Clean Higienização</title>
      </Head>

      <Header />

      <main>
        <Home />
        <About />
        <Storelist />
        <Contact />
      </main>

      <ScrollBack />
    </>
  );
}
