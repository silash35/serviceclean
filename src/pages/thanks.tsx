import Head from "next/head";

import Header from "../components/header";
import Thanks from "../components/thanks";

export default function HomePage() {
  return (
    <>
      <Head>
        <title>Obrigado</title>
      </Head>

      <Header />

      <main>
        <Thanks />
      </main>
    </>
  );
}
