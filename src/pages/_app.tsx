import "../styles/globals.scss";

import type { AppProps } from "next/app";
import Head from "next/head";

export default function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        {/* Viewport meta tag should not be used in _document.tsx. That's why it's in this file */}
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        {/* These meta tags need to be here for the key attribute to work properly */}
        <meta property="og:title" content="Service Clean Higienização" key="ogTitle" />
        <meta name="twitter:title" content="Service Clean Higienização" key="twitterTitle" />
        <meta
          property="og:description"
          content="Empresa especializada em higienização e impermeabilização de estofados"
          key="ogDescription"
        />
        <meta
          name="twitter:description"
          content="Empresa especializada em higienização e impermeabilização de estofados"
          key="twitterDescription"
        />
      </Head>
      <Component {...pageProps} />
    </>
  );
}
