import Screen from "../screen";
import styles from "./contact.module.scss";
import Form from "./form";
import Shape from "./shape";
import Title from "./title";

export default function Contact() {
  return (
    <Screen id="contact" background="gradient">
      <Shape />

      <div className={styles.flex}>
        <Title />
        <Form />
      </div>
    </Screen>
  );
}
