import ImageModal from "../imageModal";
import styles from "./title.module.scss";

export default function Title() {
  return (
    <section className={styles.title}>
      <h2>Seja nosso Cliente</h2>
      <p>Faça um orçamento com a gente, sem compromisso</p>

      <ImageModal
        src="resources/aline.webp"
        alt="Foto de Aline, uma funcionaria da Service Clean, fazendo a higienização de um sofá"
        loading="lazy"
      />
    </section>
  );
}
