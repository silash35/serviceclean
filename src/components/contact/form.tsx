import styles from "./form.module.scss";

export default function Form() {
  return (
    <section>
      <h3 className={styles.title}>Entre em Contato</h3>
      <form
        className={styles.form}
        method="POST"
        action="https://formsubmit.co/servclean001@gmail.com"
        encType="multipart/form-data"
      >
        <div className={styles.textField}>
          <label htmlFor="name">Nome*</label>
          <input id="name" name="name" required />
        </div>
        <div className={styles.textField}>
          <label htmlFor="email">Email*</label>
          <input type="email" id="email" name="email" required />
        </div>

        <div className={styles.textField}>
          <label htmlFor="tel">Telefone</label>
          <input type="tel" id="tel" name="tel" />
        </div>
        <div className={styles.textField}>
          <label htmlFor="address">Endereço*</label>
          <input id="address" name="address" required />
        </div>

        <div className={`${styles.textField} ${styles.gridLargeItem}`}>
          <label htmlFor="message">Gostaria de fazer alguma observação?</label>
          <textarea aria-label="Insira sua mensagem" id="message" name="message" />
        </div>
        <div className={` ${styles.file} ${styles.gridLargeItem}`}>
          <label htmlFor="file">
            Envie uma foto do seu estofado*
            <div>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                version="1.1"
                width="24"
                height="24"
                viewBox="0 0 24 24"
              >
                <path d="M9,16V10H5L12,3L19,10H15V16H9M5,20V18H19V20H5Z" />
              </svg>
              Fazer Upload
            </div>
          </label>
          <input type="file" id="file" name="mail" accept="image/*" required />
        </div>

        <button type="submit" className={styles.send}>
          <img src="/resources/icons/send.svg" alt="paper plane icon" />
          Enviar
        </button>

        {/* FormSubmit config */}

        <input type="hidden" name="_next" value="https://www.serviceclean.net.br/thanks" />
      </form>
    </section>
  );
}
