import styles from "./imageModal.module.scss";

export default function ImageModal(props: React.ImgHTMLAttributes<HTMLImageElement>) {
  return (
    <div className={styles.imageModal}>
      <img {...props} width="360" />
    </div>
  );
}
