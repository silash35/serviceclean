import styles from "./screen.module.scss";

interface Props {
  children: React.ReactNode;
  background?: string;
  otherStyles?: string;
  id: string;
}

export default function Screen({ children, background, otherStyles, id }: Props) {
  return (
    <article
      id={id}
      className={`${styles.screen} ${background ? styles[background] : undefined} ${otherStyles}`}
    >
      {children}
    </article>
  );
}
