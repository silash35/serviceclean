import ImageModal from "../imageModal";
import Screen from "../screen";
import styles from "./about.module.scss";
import Shape from "./shape";

export default function About() {
  return (
    <Screen id="about" background="gradient" otherStyles={styles.about}>
      <Shape />

      <section className={styles.content}>
        <article>
          <section className={styles.title}>
            <h2>Sobre a Service Clean</h2>
          </section>
          <section className={styles.text}>
            <p>
              A service clean teve início no ano de 2020, na cidade de Cruz das Almas, com o
              objetivo de oferecer serviços de higienização de estofados, tapetes e
              impermeabilização de qualidade para nossos clientes.{" "}
            </p>
            <p>
              Somos uma empresa premium que trabalha com segurança, eficácia, produtos de qualidade
              e profissionais especializados para oferecer sempre o melhor ao nosso consumidor.
            </p>
          </section>
          <section className={styles.stars}>
            {[...Array(5)].map((e, i) => (
              <img src="resources/icons/star.svg" width={50} key={i} />
            ))}
          </section>
        </article>

        <ImageModal
          src="resources/ronie.webp"
          alt="Foto de Ronie Goes, dono e fundador da Service Clean, fazendo a higienização de uma cadeira"
          loading="lazy"
        />
      </section>
    </Screen>
  );
}
