import { useState } from "react";
import { CSSTransition, SwitchTransition } from "react-transition-group";

import Screen from "../screen";
import Brazil from "./brazil";
import styles from "./storelist.module.scss";

export default function Storelist() {
  const [state, setStates] = useState("all");

  return (
    <Screen id="storelist">
      <h2 className={styles.title}>Conheça nossas unidades</h2>

      <section className={styles.content}>
        <Brazil selectState={setStates} />
        <SwitchTransition>
          <CSSTransition
            key={state}
            timeout={300}
            classNames={{
              enter: styles.unitsEnter,
              enterActive: styles.unitsEnterActive,
              enterDone: styles.unitsEnterDone,
              exit: styles.unitsExit,
              exitActive: styles.unitsExitActive,
            }}
          >
            <ul className={styles.units}>{RenderUnits(state)}</ul>
          </CSSTransition>
        </SwitchTransition>
      </section>
    </Screen>
  );
}

const RenderUnits = (state: string) => {
  const NotFound = (
    <li className={styles.unit}>
      <h3>Oops!</h3>
      <p>
        Ainda não chegamos nessa região. Mas não fique triste, nos siga nas redes sociais saber
        quando estaremos disponíveis
      </p>
      <ul>
        <li>
          <a
            href="https://www.instagram.com/serviceclean.ba/"
            title="Instagram da Service clean Bahia"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img src="/resources/icons/instagram.svg" alt="Ícone do Instagram" />
            @serviceclean.ba
          </a>
        </li>
        <li>
          <a
            href="https://www.facebook.com/servicecleanba"
            title="Facebook da Service clean Bahia"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img src="/resources/icons/facebook.svg" alt="Ícone do Facebook" />
            @serviceclean.ba
          </a>
        </li>
      </ul>
    </li>
  );
  const BahiaUnits = (
    <>
      <li className={styles.unit}>
        <h3>Cruz das Almas - BA</h3>
        <p>
          Levando mais conforto e qualidade de vida através da higienização profissional de
          estofados para cruz das almas e regiões próximas
        </p>
        <ul>
          <li>
            <a
              href="https://www.instagram.com/serviceclean.ba/"
              title="Instagram da Service clean Bahia"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img src="/resources/icons/instagram.svg" alt="Ícone do Instagram" />
              @serviceclean.ba
            </a>
          </li>
          <li>
            <a
              href="https://api.whatsapp.com/send?phone=557583110547&text=Ol%C3%A1%2C%20gostaria%20de%20realizar%20um%20or%C3%A7amento."
              title="Whatsapp da Service clean Bahia"
              target="blank"
              rel="noopener noreferrer"
            >
              <img src="/resources/icons/whatsapp.svg" alt="Ícone Whatsapp" />
              Whatsapp
            </a>
          </li>
          <li>
            <a href="tel:+5575983110547" title="Celular da Service clean Bahia">
              <img src="/resources/icons/phone.svg" alt="Ícone de um telefone" />
              +55 75 98311-0547
            </a>
          </li>
        </ul>
      </li>
    </>
  );

  switch (state) {
    case "all":
      return BahiaUnits;

    case "Bahia":
      return BahiaUnits;

    default:
      return NotFound;
  }
};
