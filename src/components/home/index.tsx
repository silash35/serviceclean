import Screen from "../screen";
import styles from "./home.module.scss";

export default function Home() {
  return (
    <Screen id="home">
      <img className={styles.logo} alt="Ícone da Service Clean" src="/logo.svg" width="500"></img>
      <div className={styles.features}>
        <section>
          <img alt="Desenho de um borrifador" src="/resources/spray.svg"></img>
          <div>
            <h2>Limpeza profunda</h2>
            <p>
              Diga adeus aos ácaros, manchas e ao encardido. A Service Clean elimina a sujeira e
              melhora a sua qualidade de vida.
            </p>
          </div>
        </section>
        <section>
          <img alt="Desenho minimalista de uma casa" src="/resources/house.svg"></img>
          <div>
            <h2>Serviço a Domicilio</h2>
            <p>Nossa equipe vai até a sua casa ou empresa e cuidamos dos seus estofados lá mesmo</p>
          </div>
        </section>
        <section>
          <img alt="Desenho minimalista de uma casa" src="/resources/spill.svg"></img>
          <div>
            <h2>Impermeabilização profissional</h2>
            <p>Aumente a vida útil do seu estofado e o proteja contra os acidentes do dia a dia.</p>
          </div>
        </section>
      </div>
    </Screen>
  );
}
