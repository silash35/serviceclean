import Link from "next/link";
import React from "react";
import { CSSTransition } from "react-transition-group";

import styles from "./mobileMenu.module.scss";

interface Props {
  menu: boolean;
  closeMenu: () => void;
}

export default function MobileMenu({ menu, closeMenu }: Props) {
  return (
    <>
      <CSSTransition
        in={menu}
        timeout={300}
        classNames={{
          enterActive: styles.mobileMenuEnterActive,
          enterDone: styles.mobileMenuEnterDone,
          exit: styles.mobileMenuExit,
          exitActive: styles.mobileMenuExitActive,
        }}
        unmountOnExit
      >
        <div className={styles.mobileMenu}>
          <button onClick={closeMenu}>
            <img alt="Botão para fechar o Menu" src="/resources/icons/close.svg" height="50" />
          </button>

          <img
            onClick={closeMenu}
            alt="Ícone da Service Clean. O nome Service Clean escrito em um fundo azul escuro"
            src="/text.svg"
            height="64"
          />
          <Link href="/#contact" onClick={closeMenu}>
            Solicite um
            <br />
            Orçamento
          </Link>
          <Link href="/#about" onClick={closeMenu}>
            Sobre
          </Link>
          <Link href="/#storelist" onClick={closeMenu}>
            Unidades
          </Link>
          <Link href="/#contact" onClick={closeMenu}>
            Contato
          </Link>
        </div>
      </CSSTransition>
      <CSSTransition
        in={menu}
        timeout={1000}
        classNames={{
          enterActive: styles.screenDarkenerEnterActive,
          enterDone: styles.screenDarkenerEnterDone,
          exit: styles.screenDarkenerExit,
          exitActive: styles.screenDarkenerExitActive,
        }}
        unmountOnExit
      >
        <div className={styles.screenDarkener}></div>
      </CSSTransition>
    </>
  );
}
