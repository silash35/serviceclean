import Link from "next/link";
import { useState } from "react";

import styles from "./header.module.scss";
import MobileMenu from "./mobileMenu";

export default function Header() {
  const [menu, setMenu] = useState(false);

  const openMenu = () => {
    setMenu(true);
  };

  const closeMenu = () => {
    setMenu(false);
  };

  return (
    <header className={styles.header}>
      <nav>
        <div className={styles.left}>
          <div className={styles.image}>
            <Link href="/">
              <img
                alt="Ícone da Service Clean. O nome Service Clean escrito em um fundo azul escuro"
                src="/text.svg"
                height="64"
              />
            </Link>
          </div>

          <div className={styles.links}>
            <Link href="/#about">Sobre</Link>
            <Link href="/#storelist">Unidades</Link>
            <Link href="/#contact">Contato</Link>
          </div>
        </div>

        <div className={styles.budget}>
          <Link href="/#contact">
            Solicite um <br /> Orçamento
          </Link>
        </div>

        <div className={styles.mobile}>
          <button onClick={openMenu}>
            <img alt="Botão para abrir o Menu" src="/resources/icons/menu.svg" height="50" />
          </button>
        </div>

        <MobileMenu menu={menu} closeMenu={closeMenu} />
      </nav>
    </header>
  );
}
