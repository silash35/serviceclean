import Link from "next/link";

import styles from "./thanks.module.scss";

export default function Thanks() {
  return (
    <div className={styles.thanks}>
      <img alt="Ícone de verificado azul" src="/resources/icons/check-circle.svg" width="400" />
      <h1>Recebemos sua mensagem!</h1>
      <p>
        Obrigado por nos dar uma chance. Agora é só esperar, que em breve um dos nossos
        especialistas irá entrar em contato com você.
      </p>
      <Link href="/">Voltar para o site</Link>
    </div>
  );
}
