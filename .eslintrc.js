/* eslint-disable @typescript-eslint/no-var-requires */
const eslintConfig = require("eslint-config-silash35");

eslintConfig.rules["react/prop-types"] = "off";

module.exports = eslintConfig;
